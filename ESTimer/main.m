//
//  main.m
//  ESTimer
//
//  Created by Paul on 2018/6/7.
//  Copyright © 2018 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#if CODECOVERAGE
#import <RCodeCoverage/GCDAProfiling.h>
#endif
int main(int argc, char * argv[]) {
    @autoreleasepool {
#if CODECOVERAGE
        CODE_CCOVER_START
#endif
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
